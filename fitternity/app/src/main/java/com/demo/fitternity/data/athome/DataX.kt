package com.demo.fitternity.data.athome

data class DataX(
    val category: String,
    val difficulty_level: Int,
    val difficulty_level_str: String,
    val duration: Int,
    val id: Int,
    val image: String,
    val slug: String,
    val title: String,
    val trainer: String
)