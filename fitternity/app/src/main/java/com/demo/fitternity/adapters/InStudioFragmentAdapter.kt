package com.demo.fitternity.adapters

import com.demo.fitternity.R
import com.demo.fitternity.common.BaseRecyclerViewAdapter

class InStudioFragmentAdapter : BaseRecyclerViewAdapter<String>() {

    enum class section_order{
        campaigns,
        onepass_pre,
        personal_training,
        upcoming_classes,
        challenge,
        fit_tv
    }

    private lateinit var inStudioList :List<String>

    override fun getLayoutIdForPosition(position: Int): Int {
        when {
            section_order.campaigns.name.equals(inStudioList[position]) -> {
                return R.layout.campaigns_layout
            }
            section_order.onepass_pre.name.equals(inStudioList[position]) -> {
                return R.layout.onepass_pre_layout
            }
            section_order.personal_training.name.equals(inStudioList[position]) -> {
                return R.layout.personal_training_layout
            }
            section_order.upcoming_classes.name.equals(inStudioList[position]) -> {
                return R.layout.upcomming_classes
            }
            section_order.challenge.name.equals(inStudioList[position]) -> {
                return R.layout.challenge_layout
            }
            section_order.fit_tv.name.equals(inStudioList[position]) -> {
               return R.layout.fit_tv_layout
            }
        }
        return R.layout.fit_tv_layout
    }

    override fun getItemCount(): Int {
       return inStudioList.size
    }

    override fun getObjForPosition(position: Int): String {
       return inStudioList[position]
    }

    override fun onItemClick(item: String?) {
    }

    fun setInStudioList(inStudioList: List<String>){
        this.inStudioList = inStudioList
        notifyDataSetChanged()
    }
}