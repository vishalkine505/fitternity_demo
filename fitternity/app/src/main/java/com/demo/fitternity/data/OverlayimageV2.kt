package com.demo.fitternity.data

data class OverlayimageV2(
    val icon: String,
    val text: String
)