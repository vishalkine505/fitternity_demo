package com.demo.fitternity.data

data class CampaignXXXXX(
    val bg_color: String,
    val image: String,
    val text_color: String,
    val title: String,
    val url: String
)