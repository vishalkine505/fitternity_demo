package com.demo.fitternity.data.atstudio

import com.demo.fitternity.data.CampaignX

data class Categories(
    val all_category_title: String,
    val campaign: CampaignX,
    val categorytags: List<Categorytag>,
    val max_category: Int,
    val text: String,
    val title: String
)