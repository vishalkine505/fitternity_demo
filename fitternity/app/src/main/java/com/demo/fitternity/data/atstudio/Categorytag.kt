package com.demo.fitternity.data.atstudio

data class Categorytag(
    val _id: Int,
    val image: String,
    val name: String,
    val slug: String
)