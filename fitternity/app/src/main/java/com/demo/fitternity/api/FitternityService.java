package com.demo.fitternity.api;



import com.demo.fitternity.data.athome.AtHomeResponse;
import com.demo.fitternity.data.atstudio.AtStudioResponse;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;


/** REST end points */
public interface FitternityService {

  @GET("homescreenapi/HomeScreenInStudio.json")
  Call<AtStudioResponse> getHomeScreenInStudio();

  @GET("homescreenapi/HomeScreenAtHome.json")
  Call<AtHomeResponse> getHomeScreenAtHome();

}
