package com.demo.fitternity.data

data class ProductTag(
    val image: String,
    val link: String,
    val text: String,
    val title: String
)