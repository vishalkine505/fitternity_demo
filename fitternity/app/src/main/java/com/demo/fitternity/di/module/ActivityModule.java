package com.demo.fitternity.di.module;

import com.demo.fitternity.common.BaseActivity;
import com.demo.fitternity.ui.activity.DashboardActivity;
import com.demo.fitternity.ui.activity.SplashscreenActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class ActivityModule {

  @ContributesAndroidInjector(modules = FragmentsModule.class)
  abstract BaseActivity contributeBaseActivity();

  @ContributesAndroidInjector
  abstract SplashscreenActivity contributeSplashActivity();

  @ContributesAndroidInjector
  abstract DashboardActivity contributesDashboardActivity();

}
