package com.demo.fitternity.extentions

import android.os.Build
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation

/**
 * @param resource sets the background resource on the ImageView
 */
inline fun <T: ImageView>T.loadImageResource(@DrawableRes resource: Int){
    setBackgroundResource(resource)
}

/**
 * @param resource sets the background drawable on the ImageView
 */
inline fun <T: ImageView>T.loadBackground(@DrawableRes resource: Int){
    val drawable = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        context.resources.getDrawable(resource, context.theme)
    else
        context.resources.getDrawable(resource)

    background = drawable
}

inline fun <T: ImageView>T.setImageResource(imageResName: String){
    val identifier = context.resources.getIdentifier(imageResName, "drawable", context.packageName)
    if(identifier != 0){
        loadImageResource(identifier)
    }
}

inline fun <T: ImageView> T.loadImage(url: String, @DrawableRes defaultImage: Int, transformation: Transformation? = null){
    if(!url.isEmpty())
        Picasso.get().load(url).placeholder(defaultImage).error(defaultImage).into(this)
    else
        setImageResource(defaultImage)
}