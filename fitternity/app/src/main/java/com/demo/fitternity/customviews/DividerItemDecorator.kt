package com.app.ob.movetshop.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.demo.fitternity.R


class DividerItemDecorator(private val context: Context): RecyclerView.ItemDecoration() {

    private val divider: Drawable by lazy { if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        context.resources.getDrawable(R.drawable.bg_item_decor_line)
    else
        context.resources.getDrawable(R.drawable.bg_item_decor_line, context.theme) }

    private val leftRightPadding: Int by lazy {
        context.resources.getDimensionPixelSize(R.dimen._2sdp)
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val dividerLeft = parent.paddingLeft + leftRightPadding
        val dividerRight = parent.width - parent.paddingRight - leftRightPadding
        val childCount = parent.childCount

        for(i in 0..childCount-2){
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams

            val dividerTop = child.bottom + params.bottomMargin
            val dividerBottom = dividerTop + divider.intrinsicHeight

            divider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)

            divider.draw(c)
        }
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.getChildAdapterPosition(view) == state.itemCount - 1) {
            outRect.setEmpty()
        } else
            outRect.set(0, 0, 0, divider.intrinsicHeight)
    }
}