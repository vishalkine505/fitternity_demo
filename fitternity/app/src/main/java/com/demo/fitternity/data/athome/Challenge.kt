package com.demo.fitternity.data.athome

import com.demo.fitternity.data.CampaignX

data class Challenge(
    val campaign: CampaignX,
    val `data`: List<Data>,
    val description: String,
    val title: String
)