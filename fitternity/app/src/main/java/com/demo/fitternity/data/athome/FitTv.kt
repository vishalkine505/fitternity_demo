package com.demo.fitternity.data.athome

import com.demo.fitternity.data.CampaignXX

data class FitTv(
    val button_text: String,
    val campaign: CampaignXX,
    val `data`: List<DataX>,
    val description: String,
    val title: String
)