package com.demo.fitternity.di.module;

import android.app.Application;
import android.content.Context;

import com.demo.fitternity.api.FitternityService;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class AppModule {

  @Provides
  @Singleton
  FitternityService provideApiService(Retrofit retrofit) {
    return retrofit.create(FitternityService.class);
  }

  @Provides
  Executor provideExecutor() {
    int numberOfCores = Runtime.getRuntime().availableProcessors();
    return Executors.newFixedThreadPool(numberOfCores * 2);
  }

  @Provides
  @Singleton
  Context provideContext(Application application) {
    return application;
  }
}
