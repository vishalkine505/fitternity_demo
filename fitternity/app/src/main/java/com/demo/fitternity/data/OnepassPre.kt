package com.demo.fitternity.data

data class OnepassPre(
    val button_text: String,
    val campaign: CampaignXXX,
    val header_img: String,
    val passes: Passes
)