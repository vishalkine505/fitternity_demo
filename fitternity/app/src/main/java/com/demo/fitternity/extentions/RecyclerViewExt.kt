package com.demo.fitternity.extentions

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.app.ob.movetshop.customview.DividerItemDecorator

inline fun <VH: RecyclerView.ViewHolder, T: RecyclerView.Adapter<VH>>RecyclerView.setupVerticalRecyclerView(adapter: T, shouldHaveDecor: Boolean = true){
    val lm = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    if(shouldHaveDecor)
        addItemDecoration(DividerItemDecorator(MenuFragment@ this.context!!))
    this.adapter = adapter
    layoutManager = lm
}

inline fun <VH: RecyclerView.ViewHolder, T: RecyclerView.Adapter<VH>>RecyclerView.setupHorizontalRecyclerView(adapter: T, snapHelper: SnapHelper? = null){
    val lm = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
    this.adapter = adapter
    layoutManager = lm
    snapHelper?.attachToRecyclerView(this)

}

inline fun <VH: RecyclerView.ViewHolder, T: RecyclerView.Adapter<VH>>RecyclerView.setupGridRecyclerView(adapter: T, spanCount: Int){
    val lm = GridLayoutManager(context, spanCount)
    this.adapter = adapter
    layoutManager = lm
}
