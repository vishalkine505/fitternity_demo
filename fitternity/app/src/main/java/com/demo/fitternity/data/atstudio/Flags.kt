package com.demo.fitternity.data.atstudio

data class Flags(
    val april5: Boolean,
    val coming_on_onepass: Boolean,
    val covid_state: String,
    val covid_state_id: Int,
    val covid_state_immediately: Boolean,
    val featured: Boolean,
    val forced_on_onepass: Boolean,
    val hyper_local_list: List<String>,
    val lite_classpass_available: Boolean,
    val membership: String,
    val monsoon_flash_discount: String,
    val monsoon_flash_discount_disabled: Boolean,
    val monsoon_flash_discount_per: Int,
    val newly_launched: Boolean,
    val not_available_on_onepass: Boolean,
    val opening_soon: Boolean,
    val state: String,
    val top_selling: Boolean,
    val trial: String
)