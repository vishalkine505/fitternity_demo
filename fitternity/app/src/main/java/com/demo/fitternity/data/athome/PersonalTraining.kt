package com.demo.fitternity.data.athome

import com.demo.fitternity.data.CampaignXXXX

data class PersonalTraining(
    val button_text: String,
    val campaign: CampaignXXXX,
    val description: String,
    val image: String,
    val link: String,
    val subtitle: String,
    val title: String
)