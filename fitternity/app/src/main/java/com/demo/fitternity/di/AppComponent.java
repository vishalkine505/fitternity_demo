package com.demo.fitternity.di;

import android.app.Application;

import com.demo.fitternity.di.module.ActivityModule;
import com.demo.fitternity.di.module.AppModule;
import com.demo.fitternity.di.module.FragmentsModule;
import com.demo.fitternity.di.module.NetworkModule;
import com.demo.fitternity.di.module.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Singleton
@Component(
    modules = {
      AndroidSupportInjectionModule.class,
      AppModule.class,
      NetworkModule.class,
      ViewModelModule.class,
      ActivityModule.class,
      FragmentsModule.class
    })
public interface AppComponent extends AndroidInjector<DaggerApplication> {
  @Component.Builder
  interface Builder {

    AppComponent build();

    @BindsInstance
    AppComponent.Builder application(Application application);
  }


}
