package com.demo.fitternity.data.atstudio

data class Address(
    val landmark: String,
    val line1: String,
    val line2: String,
    val line3: String,
    val location: String,
    val pincode: Int
)