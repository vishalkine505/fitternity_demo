package com.demo.fitternity.customviews

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

class FitternityTextView: AppCompatTextView {
    constructor(context: Context): super(context)
    constructor(context: Context, attrSet: AttributeSet): super(context, attrSet)
    constructor(context: Context, attrSet: AttributeSet, defAttrStyle: Int): super(context, attrSet, defAttrStyle)
}