package com.demo.fitternity.ui.fragment


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.demo.fitternity.R
import com.demo.fitternity.adapters.InStudioFragmentAdapter
import com.demo.fitternity.databinding.DashboardActivityBinding
import com.demo.fitternity.databinding.FragmentInStudioBinding
import com.demo.fitternity.di.FitternityViewModelFactory
import com.demo.fitternity.extentions.setupHorizontalRecyclerView
import com.demo.fitternity.extentions.setupVerticalRecyclerView
import com.demo.fitternity.ui.viewmodels.HomeViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class InStudioFragment:DaggerFragment(),SwipeRefreshLayout.OnRefreshListener {

    companion object {
        private val TAG = this.javaClass.name
    }

    private lateinit var inStudioBinding: FragmentInStudioBinding

    private lateinit var viewModel: HomeViewModel
    @Inject
    lateinit var factory: FitternityViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inStudioBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_in_studio, container, false);
        inStudioBinding.lifecycleOwner = viewLifecycleOwner
        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        viewModel.fetchInStudio()
        viewModel.inStudioResponse?.observe(viewLifecycleOwner, Observer {
            if(it!=null && it.section_orders.isNotEmpty()){
                it.section_orders.forEach { name ->
                    val adapter = InStudioFragmentAdapter()
                    adapter.setInStudioList(inStudioList = it.section_orders)
                    inStudioBinding.mainRecyclerview.setupVerticalRecyclerView(adapter)
                }
            }
        })

        return inStudioBinding.root
    }

    override fun onRefresh() {
        viewModel.fetchInStudio()
    }


}