package com.demo.fitternity.di.module;

import com.demo.fitternity.common.BaseFragment;
import com.demo.fitternity.ui.fragment.AtHomeFragment;
import com.demo.fitternity.ui.fragment.InStudioFragment;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentsModule {

  @ContributesAndroidInjector
  abstract AtHomeFragment contributesAtHomeFragment();

  @ContributesAndroidInjector
  abstract InStudioFragment contributesInStudioFragment();
}
