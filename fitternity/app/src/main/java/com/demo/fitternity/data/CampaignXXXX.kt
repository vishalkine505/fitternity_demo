package com.demo.fitternity.data

data class CampaignXXXX(
    val bg_color: String,
    val image: String,
    val text_color: String,
    val title: String,
    val url: String
)