package com.demo.fitternity.extentions

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

/**
 * @param function is a high order lambda function that would to be executed on the Fragment transaction
 */
inline fun FragmentManager.inTransaction(function: FragmentTransaction.() -> FragmentTransaction){
    val ft = beginTransaction()
    ft.function()
    ft.commit()
}