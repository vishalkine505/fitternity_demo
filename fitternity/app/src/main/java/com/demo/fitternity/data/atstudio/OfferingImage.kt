package com.demo.fitternity.data.atstudio

data class OfferingImage(
    val height: Int,
    val image: String,
    val width: Int
)