package com.demo.fitternity.data.atstudio

import com.demo.fitternity.data.Campaign
import com.demo.fitternity.data.OnepassPre
import com.demo.fitternity.data.ProductTag
import com.demo.fitternity.data.UpcomingClasses

data class AtStudioResponse(
    val campaigns: List<Campaign>,
    val categories: Categories,
    val city_id: Int,
    val city_name: String,
    val fitness_centres: FitnessCentres,
    val instudio: String,
    val onepass_pre: OnepassPre,
    val product_tags: List<ProductTag>,
    val section_orders: List<String>,
    val upcoming_classes: UpcomingClasses
)