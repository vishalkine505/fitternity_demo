package com.demo.fitternity.data.atstudio

data class FitnessCentres(
    val button_text: String,
    val `data`: List<Data>,
    val description: String,
    val title: String
)