package com.demo.fitternity.data.atstudio

import com.demo.fitternity.data.OverlayDetail
import com.demo.fitternity.data.OverlayimageV2

data class DataX(
    val address: Address,
    val average_rating: Double,
    val booking_points: Int,
    val campaign_text: String,
    val commercial: String,
    val coverimage: String,
    val flags: Any,
    val id: Int,
    val location: String,
    val name: String,
    val next_slot: String,
    val overlay_details: List<OverlayDetail>,
    val overlayimage: String,
    val overlayimage_v2: OverlayimageV2,
    val pps_oneliner: String,
    val service_type: String,
    val session_time: Int,
    val slug: String,
    val special_price: Int,
    val tag_image: String,
    val total_rating_count: Int,
    val total_slots: String,
    val vendor_name: String,
    val vendor_slug: String
)