package com.demo.fitternity.ui.activity

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.demo.fitternity.R
import com.demo.fitternity.common.BaseActivity
import com.demo.fitternity.extentions.loadBackground
import com.demo.fitternity.extentions.setFullScreen
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.*

class SplashscreenActivity : DaggerAppCompatActivity() {

    private val activityScope = CoroutineScope(Dispatchers.Main)
    private lateinit var splashImage:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        setFullScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        loadSplashImage()
        loadHomeDashboard()
    }

    private fun loadSplashImage(){
        splashImage = findViewById(R.id.splash_img)
        splashImage.loadBackground(R.drawable.splash_screen)
    }

    private fun loadHomeDashboard() {
         activityScope.launch {
             delay(3000)
             val intent = Intent(this@SplashscreenActivity, DashboardActivity::class.java)
             startActivity(intent)
             finish()
         }
    }

    override fun onPause() {
        activityScope.cancel()
        super.onPause()
    }

}