package com.demo.fitternity.extentions

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.AnimRes
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.demo.fitternity.utils.AnimationListener

inline fun <T: FragmentActivity>T.addFragment(
    fragment: Fragment, @IdRes containerId: Int,
    tag: String? = null, backStackTag: String? = null
) {
    supportFragmentManager.inTransaction {
        if (tag != null && backStackTag != null){
            add(containerId, fragment, tag)
            addToBackStack(backStackTag)
        }else if(tag != null){
            add(containerId, fragment, tag)
        } else add(containerId, fragment)
    }
}

inline fun <T: FragmentActivity>T.replaceFragment(
    fragment: Fragment, @IdRes containerId: Int,
    backStackTag: String? = null
) {
    supportFragmentManager.inTransaction {
        backStackTag?.let {
            addToBackStack(it)
        }
        replace(containerId, fragment)
    }
}

inline fun <T: Fragment>T.replaceFragmentInFragment(
    fragment: Fragment, @IdRes containerId: Int,
    backStackTag: String? = null
) {
    childFragmentManager.inTransaction {
        backStackTag?.let {
            addToBackStack(it)
        }
        replace(containerId, fragment)
    }
}



inline fun <T: AppCompatActivity>T.findFragmentByTag(tag: String): Fragment? {
    return supportFragmentManager.findFragmentByTag(tag)
}

inline fun <T: AppCompatActivity>T.isFragmentVisible(fragment: Fragment): Boolean {
    return fragment != null && fragment.isVisible
}

inline fun <T: AppCompatActivity>T.openFragmentWithAnimation(
    @AnimRes startAnim: Int, @IdRes containerId: Int, fragment: Fragment, tag: String
) {
    val ft = supportFragmentManager.beginTransaction()
    ft.setCustomAnimations(startAnim, 0)
    ft.add(containerId, fragment, tag)
    ft.commitAllowingStateLoss()
}

inline fun <T: AppCompatActivity>T.closeFragmentWithAnimation(
    @AnimRes endAnim: Int, fragment: Fragment, onAnimationEndListener: AnimationListener? = null
) {
    if (isFragmentVisible(fragment)) {
        val animation = AnimationUtils.loadAnimation(this, endAnim)
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}

            override fun onAnimationEnd(animation: Animation?) {
                if (isFragmentVisible(fragment)) {
                    supportFragmentManager.inTransaction { remove(fragment) }
                }

                onAnimationEndListener?.onAnimationEnd()
            }

            override fun onAnimationStart(animation: Animation?) {}
        })

        fragment.view?.startAnimation(animation)
    }
}

inline fun <T: AppCompatActivity> T.setFullScreen(){
    requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)
    window.setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN, android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN)
}


inline fun <T: AppCompatActivity> T.launchUri(uri: String){
    try {
        startActivity(Intent().apply {
            data = Uri.parse(uri)
            action = Intent.ACTION_VIEW
        })
    }catch(e: ActivityNotFoundException){
        Log.i("ActivityExt", "Could not launch the Uri:$uri ")
    }
}
