package com.demo.fitternity.data

import com.demo.fitternity.data.athome.DataXX

data class UpcomingClasses(
    val button_text: String,
    val campaign: CampaignXXXXX,
    val `data`: List<DataXX>,
    val description: String,
    val session_type: String,
    val title: String
)