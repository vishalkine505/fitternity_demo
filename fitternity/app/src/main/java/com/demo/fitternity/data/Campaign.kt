package com.demo.fitternity.data

data class Campaign(
    val height: Int,
    val image: String,
    val link: String,
    val order: Int,
    val ratio: Double,
    val title: String,
    val width: Int
)