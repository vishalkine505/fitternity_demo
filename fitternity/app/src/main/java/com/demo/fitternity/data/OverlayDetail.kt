package com.demo.fitternity.data

data class OverlayDetail(
    val icon: String,
    val text: String
)