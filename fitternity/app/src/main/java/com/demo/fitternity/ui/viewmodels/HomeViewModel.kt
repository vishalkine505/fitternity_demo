package com.demo.fitternity.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.demo.fitternity.data.athome.AtHomeResponse
import com.demo.fitternity.data.atstudio.AtStudioResponse
import com.demo.fitternity.repository.HomeRepository
import javax.inject.Inject

class HomeViewModel @Inject constructor(var homeRespository: HomeRepository?) : ViewModel() {
    companion object {
        private val TAG = HomeViewModel::class.java.simpleName
    }

    private val _inStudioResponse: MutableLiveData<AtStudioResponse> = MutableLiveData()

    val inStudioResponse: LiveData<AtStudioResponse>? = homeRespository?.getStudioResponse()

    val _atHomeResponse: MutableLiveData<AtHomeResponse> = MutableLiveData()

    val atHomeResponse: LiveData<AtHomeResponse>? = homeRespository?.getHomeRespone()

    fun fetchAtHome() {
         homeRespository?.fetchAtHomeData()

    }

    fun fetchInStudio() {
        homeRespository?.fetchAtStudioData()
    }
}