package com.demo.fitternity.data.atstudio

data class Data(
    val abw_vendor: Any,
    val address: String,
    val average_rating: Double,
    val category: String,
    val categorytags: List<String>,
    val commercial: String,
    val coverimage: String,
    val featured: Boolean,
    val flags: Flags,
    val id: Int,
    val location: String,
    val membership_header: String,
    val membership_icon: String,
    val membership_offer: String,
    val membership_offer_default: Boolean,
    val offering_images: List<OfferingImage>,
    val slug: String,
    val subcategories: List<String>,
    val tag_image: String,
    val title: String,
    val total_rating_count: Int,
    val trial_header: String,
    val type: String
)