package com.demo.fitternity.data.athome

import com.demo.fitternity.data.Campaign
import com.demo.fitternity.data.OnepassPre
import com.demo.fitternity.data.ProductTag
import com.demo.fitternity.data.UpcomingClasses

data class AtHomeResponse(
    val campaigns: List<Campaign>,
    val challenge: Challenge,
    val city_id: Int,
    val city_name: String,
    val fit_tv: FitTv,
    val instudio: String,
    val onepass_pre: OnepassPre,
    val personal_training: PersonalTraining,
    val product_tags: List<ProductTag>,
    val section_orders: List<String>,
    val upcoming_classes: UpcomingClasses
)