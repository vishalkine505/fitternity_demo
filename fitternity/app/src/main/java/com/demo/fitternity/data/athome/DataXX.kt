package com.demo.fitternity.data.athome

import com.demo.fitternity.data.OverlayDetail
import com.demo.fitternity.data.OverlayimageV2

data class DataXX(
    val average_rating: Double,
    val best_seller: Boolean,
    val booking_points: Int,
    val city: String,
    val commercial: String,
    val coverimage: String,
    val flags: Any,
    val id: Int,
    val live_location: String,
    val live_location_icon: String,
    val name: String,
    val next_slot: String,
    val overlay_details: List<OverlayDetail>,
    val overlayimage: String,
    val overlayimage_v2: OverlayimageV2,
    val pps_oneliner: String,
    val service_type: String,
    val session_time: Int,
    val slug: String,
    val special_price: Int,
    val total_rating_count: Int,
    val vendor_name: String,
    val vendor_slug: String
)