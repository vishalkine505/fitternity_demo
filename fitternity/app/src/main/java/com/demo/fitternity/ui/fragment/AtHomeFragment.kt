package com.demo.fitternity.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.demo.fitternity.R
import com.demo.fitternity.common.BaseFragment
import com.demo.fitternity.databinding.FragmentAtHomeBinding
import com.demo.fitternity.di.FitternityViewModelFactory
import com.demo.fitternity.ui.viewmodels.HomeViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class AtHomeFragment:DaggerFragment(),SwipeRefreshLayout.OnRefreshListener{

    companion object {
        private val TAG = this.javaClass.name
    }

    private lateinit var viewModel: HomeViewModel
    @Inject
    lateinit var factory:FitternityViewModelFactory

    private  lateinit var binding:FragmentAtHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
    binding =
        DataBindingUtil.inflate(inflater, R.layout.fragment_at_home, container, false);
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        viewModel.fetchAtHome()

        return binding.root
    }

    override fun onRefresh() {
        viewModel.fetchAtHome()
    }


}