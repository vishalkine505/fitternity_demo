package com.demo.fitternity.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.demo.fitternity.api.FitternityService
import com.demo.fitternity.data.athome.AtHomeResponse
import com.demo.fitternity.data.atstudio.AtStudioResponse
import com.demo.fitternity.extentions.onEnqueue
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HomeRepository@Inject constructor(val service: FitternityService) {
    companion object {
        private val TAG = this.javaClass.name
    }

    private val atStudioResponse: MutableLiveData<AtStudioResponse> =
        MutableLiveData()

    private val atHomeResponse: MutableLiveData<AtHomeResponse> =
        MutableLiveData()

    public fun getStudioResponse():LiveData<AtStudioResponse>{
        return atStudioResponse;
    }

    public fun getHomeRespone():LiveData<AtHomeResponse>{
        return atHomeResponse;
    }

     fun fetchAtHomeData():MutableLiveData<AtHomeResponse> {
        service.homeScreenAtHome.onEnqueue {
            onResponse ={
                Log.i(TAG,it.body().toString())
                 atHomeResponse.postValue(it.body())
            }
            onFailure ={
                Log.i(TAG,it?.message.toString())
                atHomeResponse.postValue(null)
            }
        }
        return atHomeResponse
    }

    fun fetchAtStudioData(): MutableLiveData<AtStudioResponse> {
        service.homeScreenInStudio.onEnqueue {
            onResponse ={
                Log.i(TAG,it.body().toString())
                atStudioResponse.postValue(it.body())
            }
            onFailure={
                Log.i(TAG,it?.message.toString())
                atStudioResponse.postValue(null)
            }
        }
       return atStudioResponse
    }

}